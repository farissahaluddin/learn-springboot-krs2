-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2019 at 11:30 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crudsboot`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_krs`
--

CREATE TABLE `table_krs` (
  `id` int(10) NOT NULL,
  `nim` int(10) NOT NULL,
  `id_matakuliah` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_krs`
--

INSERT INTO `table_krs` (`id`, `nim`, `id_matakuliah`) VALUES
(1, 20191, 102),
(3, 20193, 105),
(4, 20193, 106),
(5, 20193, 107),
(6, 20191, 104),
(7, 20191, 103),
(8, 20191, 105),
(9, 20191, 107),
(10, 20191, 101),
(13, 20192, 107),
(15, 20193, 104),
(16, 20194, 101),
(17, 20194, 103),
(18, 20194, 108),
(19, 20194, 107),
(20, 20194, 104),
(21, 20194, 106),
(22, 20191, 108),
(24, 20192, 101),
(25, 20192, 104),
(26, 20192, 102);

-- --------------------------------------------------------

--
-- Table structure for table `table_mahasiswa`
--

CREATE TABLE `table_mahasiswa` (
  `nim` int(5) NOT NULL,
  `mahasiswa` varchar(100) NOT NULL,
  `maks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_mahasiswa`
--

INSERT INTO `table_mahasiswa` (`nim`, `mahasiswa`, `maks`) VALUES
(20191, 'Widjoyo', 24),
(20192, 'Osas', 20),
(20193, 'Boni', 22),
(20194, 'Pino', 20),
(20195, 'wando', 24);

-- --------------------------------------------------------

--
-- Table structure for table `table_matakuliah`
--

CREATE TABLE `table_matakuliah` (
  `id` int(5) NOT NULL,
  `mata_kuliah` varchar(100) NOT NULL,
  `sks` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_matakuliah`
--

INSERT INTO `table_matakuliah` (`id`, `mata_kuliah`, `sks`) VALUES
(101, 'Dasar Dasar Pemrograman', 2),
(102, 'Aljabar Linier 1', 3),
(103, 'Struktur Data', 3),
(104, 'Algoritma Pemrograman', 3),
(105, 'Organisasi Komputer', 2),
(106, 'Jaringan Komputer', 3),
(107, 'Logika Matematika', 3),
(108, 'Pengantar Teknologi Informatika', 3),
(109, 'Pemrograman Berorientasi Objek', 3),
(110, 'Artificial Intelligence', 3);

-- --------------------------------------------------------

--
-- Table structure for table `table_user`
--

CREATE TABLE `table_user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user`
--

INSERT INTO `table_user` (`id`, `username`, `password`) VALUES
(5, 'paijo', 'payah'),
(6, 'userTest', 'testUser');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `table_krs`
--
ALTER TABLE `table_krs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_mahasiswa`
--
ALTER TABLE `table_mahasiswa`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `table_matakuliah`
--
ALTER TABLE `table_matakuliah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_user`
--
ALTER TABLE `table_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `table_krs`
--
ALTER TABLE `table_krs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `table_mahasiswa`
--
ALTER TABLE `table_mahasiswa`
  MODIFY `nim` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20198;

--
-- AUTO_INCREMENT for table `table_matakuliah`
--
ALTER TABLE `table_matakuliah`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `table_user`
--
ALTER TABLE `table_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
