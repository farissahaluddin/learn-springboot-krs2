package com.faris.heyspringboot.service;

import com.faris.heyspringboot.entity.Mahasiswa;

import java.util.List;

public interface MahasiswaService extends BaseService<Mahasiswa> {

    List<Mahasiswa> findByMahasiswa(Mahasiswa param);

    Mahasiswa findByNim(int nim);

}
