package com.faris.heyspringboot.service;

import com.faris.heyspringboot.entity.MataKuliah;

import java.util.List;

public interface MataKuliahService extends BaseService<MataKuliah>{
    List<MataKuliah> findByMataKuliah(MataKuliah param);
}
