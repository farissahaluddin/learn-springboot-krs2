package com.faris.heyspringboot.dao;

import com.faris.heyspringboot.entity.Mahasiswa;

import java.util.List;

public interface MahasiswaDAO extends BaseDAO<Mahasiswa>{

    List<Mahasiswa> findByMahasiswa(Mahasiswa param);

    Mahasiswa findByNim(int nim);

}
