package com.faris.heyspringboot.entity;

import javax.persistence.JoinColumn;

public class Krs {

    private Integer  id;
    private Integer  nim;
    private String mahasiswa;


    @JoinColumn(name = "id_matakuliah")
    private int idMataKuliah;

    @JoinColumn(name = "mata_kuliah")
    private String matakuliah;
    private Integer  sks;
    private Integer  maks;

    private Integer  idkrs;

    public Krs() {
    }

    public Krs(Integer id, Integer nim, String mahasiswa, int idMataKuliah, String matakuliah, Integer sks, Integer maks, Integer idkrs) {
        this.id = id;
        this.nim = nim;
        this.mahasiswa = mahasiswa;
        this.idMataKuliah = idMataKuliah;
        this.matakuliah = matakuliah;
        this.sks = sks;
        this.maks = maks;
        this.idkrs = idkrs;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNim() {
        return nim;
    }

    public void setNim(Integer nim) {
        this.nim = nim;
    }

    public String getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(String mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public int getIdMataKuliah() {
        return idMataKuliah;
    }

    public void setIdMataKuliah(int idMataKuliah) {
        this.idMataKuliah = idMataKuliah;
    }

    public String getMatakuliah() {
        return matakuliah;
    }

    public void setMatakuliah(String matakuliah) {
        this.matakuliah = matakuliah;
    }

    public Integer getSks() {
        return sks;
    }

    public void setSks(Integer sks) {
        this.sks = sks;
    }

    public Integer getMaks() {
        return maks;
    }

    public void setMaks(Integer maks) {
        this.maks = maks;
    }

    public Integer getIdkrs() {
        return idkrs;
    }

    public void setIdkrs(Integer idkrs) {
        this.idkrs = idkrs;
    }

    public void cetak(){
        System.out.println("getId(): "+getId());
        System.out.println("getNim(): "+getNim());
    }

}
